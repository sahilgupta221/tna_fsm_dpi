from copy import deepcopy
import sys
import os

# Regex validation
def is_valid_regex(regex):
    return valid_brackets(regex) and valid_operations(regex)


def valid_brackets(regex):
    opened_brackets = 0
    for c in regex:
        if c == '(':
            opened_brackets += 1
        if c == ')':
            opened_brackets -= 1
        if opened_brackets < 0:
            print('ERROR missing bracket')
            return False
    if opened_brackets == 0:
        return True
    print('ERROR unclosed brackets')
    return False


def valid_operations(regex):
    for i, c in enumerate(regex):
        if c == '*':
            if i == 0:
                print('ERROR * with no argument at', i)
                return False
            if regex[i - 1] in '(|':
                print('ERROR * with no argument at', i)
                return False
        if c == '|':
            if i == 0 or i == len(regex) - 1:
                print('ERROR | with missing argument at', i)
                return False
            if regex[i - 1] in '(|':
                print('ERROR | with missing argument at', i)
                return False
            if regex[i + 1] in ')|':
                print('ERROR | with missing argument at', i)
                return False
    return True


class RegexNode:

    @staticmethod
    def trim_brackets(regex):
        while regex[0] == '(' and regex[-1] == ')' and is_valid_regex(regex[1:-1]):
            regex = regex[1:-1]
        return regex

    @staticmethod
    def is_concat(c):
        return c == '(' or RegexNode.is_letter(c)

    @staticmethod
    def is_letter(c):
        return c in alphabet

    def __init__(self, regex):
        self.nullable = None
        self.firstpos = []
        self.lastpos = []
        self.item = None
        self.position = None
        self.children = []

        if DEBUG:
            print('Current : ' + regex)
        # Check if it is leaf
        if len(regex) == 1 and self.is_letter(regex):
            # Leaf
            self.item = regex
            # Lambda checking
            if use_lambda:
                if self.item == lambda_symbol:
                    self.nullable = True
                else:
                    self.nullable = False
            else:
                self.nullable = False
            return

        # It is an internal node
        # Finding the leftmost operators in all three
        kleene = -1
        or_operator = -1
        concatenation = -1
        i = 0

        # Getting the rest of terms
        while i < len(regex):
            if regex[i] == '(':
                # Composed block
                bracketing_level = 1
                # Skipping the entire term
                i += 1
                while bracketing_level != 0 and i < len(regex):
                    if regex[i] == '(':
                        bracketing_level += 1
                    if regex[i] == ')':
                        bracketing_level -= 1
                    i += 1
            else:
                # Going to the next char
                i += 1

            # Found a concatenation in previous iteration
            # And also it was the last element check if breaking
            if i == len(regex):
                break

            # Testing if concatenation
            if self.is_concat(regex[i]):
                if concatenation == -1:
                    concatenation = i
                continue
            # Testing for kleene
            if regex[i] == '*':
                if kleene == -1:
                    kleene = i
                continue
            # Testing for or operator
            if regex[i] == '|':
                if or_operator == -1:
                    or_operator = i

        # Setting the current operation by priority
        if or_operator != -1:
            # Found an or operation
            self.item = '|'
            self.children.append(RegexNode(self.trim_brackets(regex[:or_operator])))
            self.children.append(RegexNode(self.trim_brackets(regex[(or_operator + 1):])))
        elif concatenation != -1:
            # Found a concatenation
            self.item = '.'
            self.children.append(RegexNode(self.trim_brackets(regex[:concatenation])))
            self.children.append(RegexNode(self.trim_brackets(regex[concatenation:])))
        elif kleene != -1:
            # Found a kleene
            self.item = '*'
            self.children.append(RegexNode(self.trim_brackets(regex[:kleene])))

    def calc_functions(self, pos, followpos):
        if self.is_letter(self.item):
            # Is a leaf
            self.firstpos = [pos]
            self.lastpos = [pos]
            self.position = pos
            # Add the position in the followpos list
            followpos.append([self.item, []])
            return pos + 1
        # Is an internal node
        for child in self.children:
            pos = child.calc_functions(pos, followpos)
        # Calculate current functions

        if self.item == '.':
            # Is concatenation
            # Firstpos
            if self.children[0].nullable:
                self.firstpos = sorted(list(set(self.children[0].firstpos + self.children[1].firstpos)))
            else:
                self.firstpos = deepcopy(self.children[0].firstpos)
            # Lastpos
            if self.children[1].nullable:
                self.lastpos = sorted(list(set(self.children[0].lastpos + self.children[1].lastpos)))
            else:
                self.lastpos = deepcopy(self.children[1].lastpos)
            # Nullable
            self.nullable = self.children[0].nullable and self.children[1].nullable
            # Followpos
            for i in self.children[0].lastpos:
                for j in self.children[1].firstpos:
                    if j not in followpos[i][1]:
                        followpos[i][1] = sorted(followpos[i][1] + [j])

        elif self.item == '|':
            # Is or operator
            # Firstpos
            self.firstpos = sorted(list(set(self.children[0].firstpos + self.children[1].firstpos)))
            # Lastpos
            self.lastpos = sorted(list(set(self.children[0].lastpos + self.children[1].lastpos)))
            # Nullable
            self.nullable = self.children[0].nullable or self.children[1].nullable

        elif self.item == '*':
            # Is kleene
            # Firstpos
            self.firstpos = deepcopy(self.children[0].firstpos)
            # Lastpos
            self.lastpos = deepcopy(self.children[0].lastpos)
            # Nullable
            self.nullable = True
            # Followpos
            for i in self.children[0].lastpos:
                for j in self.children[0].firstpos:
                    if j not in followpos[i][1]:
                        followpos[i][1] = sorted(followpos[i][1] + [j])

        return pos

    def write_level(self, level):
        print(str(level) + ' ' + self.item, self.firstpos, self.lastpos, self.nullable,
              '' if self.position == None else self.position)
        for child in self.children:
            child.write_level(level + 1)


class RegexTree:

    def __init__(self, regex):
        self.root = RegexNode(regex)
        self.followpos = []
        self.functions()

    def write(self):
        self.root.write_level(0)

    def functions(self):
        positions = self.root.calc_functions(0, self.followpos)
        if DEBUG == True:
            print(self.followpos)

    def toDfa(self):

        def contains_hashtag(q):
            for i in q:
                if self.followpos[i][0] == '#':
                    return True
            return False

        M = []  # Marked states
        Q = []  # States list in the followpos form ( array of positions )
        V = alphabet - {'#', lambda_symbol if use_lambda else ''}  # Automata alphabet
        d = []  # Delta function, an array of dictionaries d[q] = {x1:q1, x2:q2 ..} where d(q,x1) = q1, d(q,x2) = q2..
        F = []  # FInal states list in the form of indexes (int)
        q0 = self.root.firstpos

        Q.append(q0)
        if contains_hashtag(q0):
            F.append(Q.index(q0))

        while len(Q) - len(M) > 0:
            # There exists one unmarked
            # We take one of those
            q = [i for i in Q if i not in M][0]
            # Generating the delta dictionary for the new state
            d.append({})
            # We mark it
            M.append(q)
            # For each letter in the automata's alphabet
            for a in V:
                # Compute destination state ( d(q,a) = U )
                U = []
                # Compute U
                # foreach position in state
                for i in q:
                    # if i has label a
                    if self.followpos[i][0] == a:
                        # We add the position to U's composition
                        U = U + self.followpos[i][1]
                U = sorted(list(set(U)))
                # Checking if this is a valid state
                if len(U) == 0:
                    # No positions, skipping, it won't produce any new states ( also won't be final )
                    continue
                if U not in Q:
                    Q.append(U)
                    if contains_hashtag(U):
                        F.append(Q.index(U))
                # d(q,a) = U
                d[Q.index(q)][a] = Q.index(U)

        return Dfa(Q, V, d, Q.index(q0), F)


class Dfa:

    def __init__(self, Q, V, d, q0, F):
        self.Q = Q
        self.V = V
        self.d = d
        self.q0 = q0
        self.F = F

    def run(self, text):
        # Checking if the input is in the current alphabet
        if len(set(text) - self.V) != 0:
            # Not all the characters are in the language
            print('ERROR characters', (set(text) - self.V), 'are not in the automata\'s alphabet')
            exit(0)

        # Running the automata
        q = self.q0
        for i in text:
            # Check if transition exists
            if q >= len(self.d):
                print('Message NOT accepted, state has no transitions')
                exit(0)
            if i not in self.d[q].keys():
                print('Message NOT accepted, state has no transitions with the character')
                exit(0)
            # Execute transition
            q = self.d[q][i]

        if q in self.F:
            print('Message accepted!')
        else:
            print('Message NOT accepted, stopped in an unfinal state')

    def write(self):
        for i in range(len(self.Q)):
            # Printing index, the delta fuunction for that transition and if it's final state
            print(i, self.d[i], 'F' if i in self.F else '')


# Preprocessing Functions
def preprocess(regex):
    regex = clean_kleene(regex)
    regex = regex.replace(' ', '')
    regex = '(' + regex + ')' + '#'
    while '()' in regex:
        regex = regex.replace('()', '')
    return regex


def clean_kleene(regex):
    for i in range(0, len(regex) - 1):
        while i < len(regex) - 1 and regex[i + 1] == regex[i] and regex[i] == '*':
            regex = regex[:i] + regex[i + 1:]
    return regex


def gen_alphabet(regex):
    return set(regex) - set('()|*')


##########################################################
#####       SAHIL FUNCTIONS STARTS
##########################################################

def ASCIItoHEX(ascii):
    # Initialize final String
    hexa = ""

    # Make a loop to iterate through
    # every character of ascii string
    for i in range(len(ascii)):
        # take a char from
        # position i of string
        ch = ascii[i]

        # cast char to integer and
        # find its ascii value
        in1 = ord(ch)

        # change this ascii value
        # integer to hexadecimal value
        part = hex(in1).lstrip("0x").rstrip("L") + ""

        # add this hexadecimal value
        # to final string.
        hexa += part

    # return the final string hex
    return hexa

def convert_to_human_readable_DFA_transition_table(DFA):
    FSM_dataplane_entries_list = ["meta.state      hdr.myhdr.byte       action"]
    is_final_state = "7"

    for current_state in range(len(DFA.d)):
        dictionary_entry=DFA.d[current_state]
        for byte_key, next_state_value in dictionary_entry.items():
            if byte_key == "@":
                byte_keyA = "0x" + ASCIItoHEX(".")
                byte_key = "."
            else:
                byte_keyA = "0x" + ASCIItoHEX(byte_key)
            if next_state_value in DFA.F:
                is_final_state = "5"
            else:
                is_final_state = "7"
            FSM_dataplane_entries_list.append(
                " " + str(
                    current_state) + "          " + byte_key + " (" + byte_keyA + ")        " + " update_state(" + str(
                    next_state_value) + "," + is_final_state + ")")

    return FSM_dataplane_entries_list


def convert_to_data_plane_rule(DFA):
    # table_add fsm_transition_table update_state {fromstate} {char} ==> {state} is_final_state (if state in minDFA.finalstates 1 else 0)
    FSM_dataplane_entries_list = []
    is_final_state = "0"

    for current_state in range(len(DFA.d)):
        dictionary_entry=DFA.d[current_state]
        for byte_key, next_state_value in dictionary_entry.items():
            if byte_key == "@":
                byte_keyA = "0x" + ASCIItoHEX(".")
                byte_key = "."
            else:
                byte_keyA = "0x" + ASCIItoHEX(byte_key)
            if next_state_value in DFA.F:
                is_final_state = "5"
            else:
                is_final_state = "7"
            FSM_dataplane_entries_list.append("fsm.add_with_update_state(pattern_state_machine_state= " + str(
                current_state) + ",byte=" + byte_keyA + ",new_state= " + str(
                next_state_value) + ",is_new_state_a_final_state=" + is_final_state + ")")

    return FSM_dataplane_entries_list


# Settings
DEBUG = False
use_lambda = False
lambda_symbol = '_'
alphabet = None

# SAHIL MAIN CODE
#regex = '(aa|b)*ab(bb|a)*'
#regex = '(google@com|facebook@com|qq@com)'

# 1. Receive input from shell script call in sys.argv[1].
print(sys.setrecursionlimit(10000))
regex = f'({sys.argv[1]})'
print(regex)

#2. Call DFA converter function
# Check
if not is_valid_regex(regex):
    exit(0)

# Preprocess regex and generate the alphabet
p_regex = preprocess(regex)
alphabet = gen_alphabet(p_regex)
# add optional letters that don't appear in the expression
extra = ''
alphabet = alphabet.union(set(extra))

# Construct
tree = RegexTree(p_regex)
if DEBUG:
    tree.write()
dfa = tree.toDfa()
print(dfa.d)
print(dfa.F)

#3. Pass the DFA table generated human redeable DFA representation
human_readable_DFA_table = convert_to_human_readable_DFA_transition_table(dfa)
print(human_readable_DFA_table)

#4. Pass the DFA table generated to DFA to BFRT API converter
bfrt_apis_list = convert_to_data_plane_rule(dfa)
print(bfrt_apis_list)
regex = regex.replace("@", ".")
#5. Create a API files and txt file for specific top n (sys.argv[2]) URL DFA API
# 5.1 : Store human readable DFA table
with open('Tofino_FSM_control_plane_rules.txt', 'a+') as filehandle:
    count = sys.argv[2]
    filehandle.write('First %s url DFA\n' % (count))
    filehandle.write('URL Pattern\n')
    filehandle.write('%s\n' % regex)
    filehandle.write('Table\n')
    filehandle.writelines("%s\n" % element for element in human_readable_DFA_table)
    filehandle.write('\n')
    filehandle.write('Data plane table entries\n')
    filehandle.writelines("%s\n" % element for element in bfrt_apis_list)
    filehandle.write('\n\n\n')

# 5.2 : Store Barefoot APIs DFA representation
count = sys.argv[2]
with open('control_plane_apis/cp_api_' + str(count) + '.txt', 'w') as mfilehandle:
    mfilehandle.writelines("%s\n" % element for element in bfrt_apis_list)
    mfilehandle.write('\n')
os.system("cat pre_setup.py >> " + "control_plane_apis/cp_api_" + str(count) + ".py")
os.system("cat " + "control_plane_apis/cp_api_" + str(count) + ".txt" + " >> " + "control_plane_apis/cp_api_" + str(
    count) + ".py")
os.system("cat post_setup.py >> " + "control_plane_apis/cp_api_" + str(count) + ".py")

#6. exit the process
exit(0)


"""
print(dfa.d[0])
print(dfa.d[0].keys())
print(dfa.d[0].values())
print([item for item in dfa.d[0].keys()])
print([item for item in dfa.d[0].values()])

# Test
message = 'yagoogle'
print('This is the regex : ' + regex)
print('This is the alphabet : ' + ''.join(sorted(alphabet)))
dfa.write()
dfa.run(message)
"""