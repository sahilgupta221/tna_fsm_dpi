bfrt.complete_operations()

# Final programming
print("""
******************* PROGAMMING RESULTS *****************
""")

print ("Table forward table:")
forward_table.dump(table=True)
print ("Table fsm_first_level_table:")
fsm_first_level_table.dump(table=True)

print ("Table fsm:")
fsm.dump(table=True)

print("Table bfrt.mirror.cfg: ")
bfrt.mirror.cfg.dump(table=True)

print("Table bfrt.tna_fsm_dpi.pipe.Ingress.let_it_go_register.get: ")
bfrt.tna_fsm_dpi.pipe.Ingress.let_it_go_register.get(REGISTER_INDEX=1)

