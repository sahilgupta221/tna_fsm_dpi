// ---------------------------------------------------------------------------
// Ingress Deparser
// ---------------------------------------------------------------------------
control IngressDeparser(packet_out pkt,
    /* User */
    inout ingress_headers_t                       hdr,
    in    ingress_metadata_t                      meta,
    /* Intrinsic */
    in    ingress_intrinsic_metadata_for_deparser_t  ig_dprsr_md)
{
        //Mirror function add it here
        Mirror() mirror;
    apply {
        // Mirror (ingress to egress) to recirculate port only for original packet

        if (ig_dprsr_md.mirror_type == MIRROR_TYPE_I2E) {
            mirror.emit<ing_port_mirror_h>(meta.ing_mir_session,{meta.mirror_header_type,0});
        }

        pkt.emit(hdr);
//        pkt.emit(hdr.ipv4);
//        pkt.emit(hdr.ipv6);
//        pkt.emit(hdr.udp);
//        pkt.emit(hdr.tcp);
//        pkt.emit(hdr.recir);
//        pkt.emit(hdr.app);
    }
}
