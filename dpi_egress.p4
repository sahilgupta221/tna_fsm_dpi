#ifndef _UTIL_
#define _UTIL_

    /***********************  P A R S E R  **************************/

parser EgressParser(
        packet_in pkt,
    /* User */
    out egress_headers_t          hdr,
    out egress_metadata_t         eg_md,
    /* Intrinsic */
    out egress_intrinsic_metadata_t  eg_intr_md)
    {
    inthdr_h inthdr;

    /* This is a mandatory state, required by Tofino Architecture */
    state start {
        pkt.extract(eg_intr_md);
        inthdr = pkt.lookahead<inthdr_h>();

        transition select(inthdr.header_type) {
            HEADER_TYPE_BRIDGE:
                           parse_bridge;
            HEADER_TYPE_MIRROR_INGRESS:
                           parse_ing_port_mirror;
            default : parse_ethernet;
        }
    }

    state parse_bridge {
        pkt.extract(eg_md.bridge);
        transition parse_ethernet;
    }

    state parse_ing_port_mirror {
        pkt.extract(eg_md.ing_port_mirror);
        transition parse_ethernet;
    }

    state parse_ethernet {
        pkt.extract(hdr.ethernet);
        transition select(hdr.ethernet.ether_type) {
            ETHERTYPE_IPV4 : parse_ipv4;
            ETHERTYPE_IPV6 : parse_ipv6;
            default : accept;
        }
    }

//ToDo: add ipv4.len field to move further only if ipv4.len >100 else accept

    state parse_ipv4 {
        pkt.extract(hdr.ipv4);
//        ipv4_checksum.add(hdr.ipv4);
        transition select(hdr.ipv4.total_len) {
           0 .. 1   : accept;
          1 .. 65535 : parse_l4_temp_state;
               default : accept;
        }
    }

    state parse_l4_temp_state {
        transition select(hdr.ipv4.protocol) {
            IP_PROTOCOLS_TCP : parse_tcp;
            IP_PROTOCOLS_UDP : parse_udp;
            default : accept;
        }
    }

    state parse_ipv6 {
        pkt.extract(hdr.ipv6);
        transition select(hdr.ipv6.next_hdr) {
            IP_PROTOCOLS_TCP : parse_tcp;
            IP_PROTOCOLS_UDP : parse_udp;
            default : accept;
        }
    }

    state parse_udp {
        pkt.extract(hdr.udp);
        transition select(hdr.udp.dst_port) {
            DNS: parse_app;
            RECIRCULAR_PORT: parse_recirculation;
            default: accept;
        }
    }

    state parse_tcp {
        pkt.extract(hdr.tcp);
        transition select(hdr.tcp.dst_port) {
            HTTP: parse_app;
            HTTPS: parse_app;
            RECIRCULAR_PORT: parse_recirculation;
            default: accept;
        }
    }

    state parse_recirculation {
        pkt.extract(hdr.recir);
        transition parse_app;
    }

    state parse_app {
        pkt.extract(hdr.app);
        transition accept;
    }

}

    /***************** M A T C H - A C T I O N  *********************/

control Egress(
    /* User */
    inout egress_headers_t                          hdr,
    inout egress_metadata_t                         eg_md,
    /* Intrinsic */
    in    egress_intrinsic_metadata_t                  eg_intr_md,
    in    egress_intrinsic_metadata_from_parser_t      eg_prsr_md,
    inout egress_intrinsic_metadata_for_deparser_t     eg_dprsr_md,
    inout egress_intrinsic_metadata_for_output_port_t  eg_oport_md)
    {
bit<16> tcp_ip_header_length;
bit<16> temp_ip_header;
bit<16> tcp_header_length;
bit<16> temp_payload;

			action mirrored_tcp_packet_action(){

			// C. Validate recirculated header
            hdr.recir.setValid();
            hdr.recir.packet_state=10;
            hdr.recir.pattern_state_machine_state=0;
			hdr.recir.let_it_go_register_value = RECIRCULATE;

			// D. Set TCP PORT to RECIRCULAR_PORT
			// Store the TCP port value in port register
			hdr.recir.port_value = hdr.tcp.dst_port;
            hdr.tcp.dst_port = 5555;
			}

			action mirrored_udp_packet_action(){

			// C. Validate recirculated header
            hdr.recir.setValid();
            hdr.recir.packet_state=10;
            hdr.recir.pattern_state_machine_state=0;
			hdr.recir.let_it_go_register_value = RECIRCULATE;

			// D. Set UDP PORT to RECIRCULAR_PORT
			// Store the UDP port value in port register
            hdr.recir.port_value = hdr.udp.dst_port;
            hdr.udp.dst_port = 5555;

             // D.1 : Set udp payload value
            //UDP payload length = hdr_length - 8
            hdr.recir.remaining_payload_length =  hdr.udp.hdr_length - 8;

			}


    apply {

         //C.  removing header from mirrored recirculated packet.
         if(hdr.recir.isValid() && hdr.recir.packet_state == 10)
         {
         hdr.app.setInvalid();
         }

        // A. Normal packet tagging
        if(eg_md.bridge.header_type  == HEADER_TYPE_BRIDGE){
        hdr.recir.packet_state = 1;
        }

        // B. Cloned packet tagging
        if( eg_md.ing_port_mirror.header_type == HEADER_TYPE_MIRROR_INGRESS && hdr.tcp.isValid()){
        mirrored_tcp_packet_action();
        }
        // B. Cloned tcp packet payload length set
        if( eg_md.ing_port_mirror.header_type == HEADER_TYPE_MIRROR_INGRESS && hdr.tcp.isValid()){
             // D.1 : Set tcp payload value
            //TCP payload length = [IP Total Length] - ( ([IP IHL] + [TCP Data offset]) * 4 )
            tcp_header_length = (bit<16>)hdr.tcp.data_offset;
            temp_ip_header = (bit<16>)hdr.ipv4.ihl;
            tcp_ip_header_length = temp_ip_header  + tcp_header_length;
        }

        if( eg_md.ing_port_mirror.header_type == HEADER_TYPE_MIRROR_INGRESS && hdr.tcp.isValid()){
            tcp_ip_header_length = tcp_ip_header_length << 2;
            hdr.recir.remaining_payload_length = hdr.ipv4.total_len;
        }

        if( eg_md.ing_port_mirror.header_type == HEADER_TYPE_MIRROR_INGRESS && hdr.tcp.isValid()){
            temp_payload = hdr.recir.remaining_payload_length;
            hdr.recir.remaining_payload_length = temp_payload - (bit<16>)tcp_ip_header_length;
        }

        if( eg_md.ing_port_mirror.header_type == HEADER_TYPE_MIRROR_INGRESS && hdr.udp.isValid()){
        mirrored_udp_packet_action();
        }
    }
}

    /*********************  D E P A R S E R  ************************/

control EgressDeparser(
        packet_out pkt,
    /* User */
    inout egress_headers_t                       hdr,
    in    egress_metadata_t                      eg_md,
    /* Intrinsic */
    in    egress_intrinsic_metadata_for_deparser_t  eg_dprsr_md)
    {
    apply {
        pkt.emit(hdr);
        //pkt.emit(hdr.ethernet);
        //pkt.emit(hdr.ipv4);
        //pkt.emit(hdr.ipv6);
        //pkt.emit(hdr.udp);
        //pkt.emit(hdr.tcp);
        //pkt.emit(hdr.recir);
        //pkt.emit(hdr.app);
    }
}


#endif /* _UTIL */
