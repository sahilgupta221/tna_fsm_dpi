#include <core.p4>
# 2 "tna_fsm_dpi.p4" 2



#include <tna.p4>
# 6 "tna_fsm_dpi.p4" 2





typedef bit<48> mac_addr_t;
typedef bit<32> ipv4_addr_t;
typedef bit<128> ipv6_addr_t;

typedef bit<16> ether_type_t;
const ether_type_t ETHERTYPE_IPV4 = 16w0x0800;
const ether_type_t ETHERTYPE_ARP = 16w0x0806;
const ether_type_t ETHERTYPE_IPV6 = 16w0x86dd;

typedef bit<8> ip_protocol_t;
const ip_protocol_t IP_PROTOCOLS_ICMP = 1;
const ip_protocol_t IP_PROTOCOLS_TCP = 6;
const ip_protocol_t IP_PROTOCOLS_UDP = 17;

const bit<8> ORIGINAL_PACKET = 0;
const bit<8> ORIGINAL_RECIRCULATED_PACKET = 1;
const bit<8> CLONED_RECIRCULATED_PACKET = 10;
const bit<8> ORIGINAL_RECIRCULATED_PACKET_PASS = 2;
const bit<8> ORIGINAL_RECIRCULATED_PACKET_FAIL = 3;
const bit<8> CLONED_RECIRCULATED_PACKET_PASS = 4;
const bit<8> CLONED_RECIRCULATED_PACKET_FAIL = 5;
const bit<8> ACCEPT = 4;
const bit<8> REJECT = 5;
const bit<8> RECIRCULATE = 7;


typedef bit<3> mirror_type_t;




const bit<8> SESSION_ID = 32;
const bit<16> RECIRCULAR_PORT = 5555;
const bit<16> HTTP = 80;
const bit<16> HTTPS = 443;
const mirror_type_t MIRROR_TYPE_I2E = 1;
const bit<16> DNS = 53;
typedef bit<8> pkt_type_t;
const pkt_type_t PKT_TYPE_MIRROR = 10;
const MirrorId_t ing_mir_ses = 0;
const pkt_type_t pkt_type = 0;


header ethernet_h {
    mac_addr_t dst_addr;
    mac_addr_t src_addr;
    bit<16> ether_type;
}

header ipv4_h {
    bit<4> version;
    bit<4> ihl;
    bit<8> diffserv;
    bit<16> total_len;
    bit<16> identification;
    bit<3> flags;
    bit<13> frag_offset;
    bit<8> ttl;
    bit<8> protocol;
    bit<16> hdr_checksum;
    ipv4_addr_t src_addr;
    ipv4_addr_t dst_addr;
}

header ipv6_h {
    bit<4> version;
    bit<8> traffic_class;
    bit<20> flow_label;
    bit<16> payload_len;
    bit<8> next_hdr;
    bit<8> hop_limit;
    ipv6_addr_t src_addr;
    ipv6_addr_t dst_addr;
}

header tcp_h {
    bit<16> src_port;
    bit<16> dst_port;
    bit<32> seq_no;
    bit<32> ack_no;
    bit<4> data_offset;
    bit<4> res;
    bit<8> flags;
    bit<16> window;
    bit<16> checksum;
    bit<16> urgent_ptr;
}

header udp_h {
    bit<16> src_port;
    bit<16> dst_port;
    bit<16> hdr_length;
    bit<16> checksum;
}

header recirculation_h {
    bit<16> pattern_state_machine_state; //state of byte matched so far from app layer
    bit<16> accept_state_flag;
    bit<8> let_it_go_register_value;
    bit<8> packet_state; //Classify packet as originally recirculated/cloned recirculated
    bit<16> port_value;
}

header app_h {
    bit<8> byte;
}

header mirror_h {
    pkt_type_t pkt_type;
}

struct ingress_headers_t {
    ethernet_h ethernet;
    ipv4_h ipv4;
    ipv6_h ipv6;
    tcp_h tcp;
    udp_h udp;
    recirculation_h recir;
    app_h app;
}

struct empty_header_t {}

struct empty_metadata_t {}
struct ingress_metadata_t {
    bit<8> packet_state; //Classify packet as originally recirculated/cloned recirculated
    bit<16> pattern_state_machine_state; //state of byte matched so far from app layer
}
struct metadata_t {}
/***********************   **************************************************
 ****************  E G R E S S   P R O C E S S I N G   *******************
 *************************************************************************/

    /***********************  H E A D E R S  ************************/

struct egress_headers_t {
    ethernet_h ethernet;
    ipv4_h ipv4;
    ipv6_h ipv6;
    tcp_h tcp;
    udp_h udp;
    recirculation_h recir;
    app_h app;
}

    /********  G L O B A L   E G R E S S   M E T A D A T A  *********/

struct egress_metadata_t {
    bit<8> packet_state; //Classify packet as originally recirculated/cloned recirculated
    bit<16> pattern_state_machine_state; //state of byte matched so far from app layer
}
# 9 "tna_fsm_dpi.p4" 2
parser TofinoIngressParser(
        packet_in pkt,
        out ingress_intrinsic_metadata_t ig_intr_md) {
    state start {
        pkt.extract(ig_intr_md);
        transition select(ig_intr_md.resubmit_flag) {
            1 : parse_resubmit;
            0 : parse_port_metadata;
        }
    }

    state parse_resubmit {
        // Parse resubmitted packet here.
        transition reject;
    }

    state parse_port_metadata {
        pkt.advance(PORT_METADATA_SIZE);
        transition accept;
    }
}

// ---------------------------------------------------------------------------
// Ingress parser
// ---------------------------------------------------------------------------
parser IngressParser(packet_in pkt,
    /* User */
    out ingress_headers_t hdr,
    out ingress_metadata_t meta,
    /* Intrinsic */
    out ingress_intrinsic_metadata_t ig_intr_md)
{

    Checksum() ipv4_checksum;
    TofinoIngressParser() tofino_parser;

    state start {
        tofino_parser.apply(pkt, ig_intr_md);
        transition parse_ethernet;
    }

    state parse_ethernet {
        pkt.extract(hdr.ethernet);
        transition select(hdr.ethernet.ether_type) {
            ETHERTYPE_IPV4 : parse_ipv4;
            ETHERTYPE_IPV6 : parse_ipv6;
            default : accept;
        }
     }

//ToDo: add ipv4.len field to move further only if ipv4.len >100 else accept

    state parse_ipv4 {
        pkt.extract(hdr.ipv4);
        ipv4_checksum.add(hdr.ipv4);
        transition select(hdr.ipv4.total_len) {
           0 .. 100 : accept;
          100 .. 65535 : parse_l4_temp_state;
               default : accept;
        }
    }

    state parse_l4_temp_state {
        transition select(hdr.ipv4.protocol) {
            IP_PROTOCOLS_TCP : parse_tcp;
            IP_PROTOCOLS_UDP : parse_udp;
            default : accept;
        }
    }

    state parse_ipv6 {
        pkt.extract(hdr.ipv6);
        transition select(hdr.ipv6.next_hdr) {
            IP_PROTOCOLS_TCP : parse_tcp;
            IP_PROTOCOLS_UDP : parse_udp;
            default : accept;
        }
    }

    state parse_udp {
        pkt.extract(hdr.udp);
        transition select(hdr.udp.src_port) {
            DNS: parse_app;
            RECIRCULAR_PORT: parse_recirculation;
            default: accept;
        }
    }

    state parse_tcp {
        pkt.extract(hdr.tcp);
        transition select(hdr.tcp.dst_port) {
            HTTP: parse_app;
            HTTPS: parse_app;
            RECIRCULAR_PORT: parse_recirculation;
            default: accept;
        }
    }

    state parse_recirculation {
        pkt.extract(hdr.recir);
        transition parse_app;
    }

    state parse_app {
        pkt.extract(hdr.app);
        transition accept;
    }

}
# 10 "tna_fsm_dpi.p4" 2
// ---------------------------------------------------------------------------
// Ingress Deparser
// ---------------------------------------------------------------------------
control IngressDeparser(packet_out pkt,
    /* User */
    inout ingress_headers_t hdr,
    in ingress_metadata_t meta,
    /* Intrinsic */
    in ingress_intrinsic_metadata_for_deparser_t ig_dprsr_md)
{
        //Mirror function add it here
        Mirror() mirror;

    apply {
        // Mirror (ingress to egress) to recirculate port only for original packet
        if (ig_dprsr_md.mirror_type == MIRROR_TYPE_I2E) {
            mirror.emit<mirror_h>(ing_mir_ses,{pkt_type});
        }

        pkt.emit(hdr.ethernet);
        pkt.emit(hdr.ipv4);
        pkt.emit(hdr.ipv6);
        pkt.emit(hdr.udp);
        pkt.emit(hdr.tcp);
        pkt.emit(hdr.recir);
        pkt.emit(hdr.app);
    }
}
# 11 "tna_fsm_dpi.p4" 2
//SwitchIngress code

control Ingress(
    /* User */
    inout ingress_headers_t hdr,
    inout ingress_metadata_t meta,
    /* Intrinsic */
    in ingress_intrinsic_metadata_t ig_intr_md,
    in ingress_intrinsic_metadata_from_parser_t ig_prsr_md,
    inout ingress_intrinsic_metadata_for_deparser_t ig_dprsr_md,
    inout ingress_intrinsic_metadata_for_tm_t ig_tm_md)
{
    // Declare register that will pass results from cloned analyzed packet to original recirculated packet
    // temparary variable to store value.
    bit<32> let_it_go_register_key=0;
    bit<8> let_it_go_register_value=0;
    bit<16> port_value=0;
    bit<8> accept_state_flag=0;
    bit<2> fsm_flag=0;

    //Registers
#ifdef CASE_FIX_2
    Register<bit<8>, bit<32>>(16384*35)  let_it_go_register;
    Register<bit<16>, bit<32>>(8192*35) port_register;
#else
    Register<bit<8>, bit<32>>(400000000) let_it_go_register;
    Register<bit<16>, bit<32>>(400000000) port_register;
#endif

#ifdef CASE_FIX_1
    RegisterAction<bit<8>, bit<32>, bit<8>>(let_it_go_register) let_it_go_read = {
        void apply(inout bit<8> reg_value, out bit<8> result) {
            result = reg_value;
        }
    };

    RegisterAction<bit<8>, bit<32>, bit<8>>(let_it_go_register) let_it_go_accept = {
        void apply(inout bit<8> reg_value) {
            reg_value = ACCEPT;
        }
    };

    RegisterAction<bit<8>, bit<32>, bit<8>>(let_it_go_register) let_it_go_reject = {
        void apply(inout bit<8> reg_value) {
            reg_value = REJECT;
        }
    };
#endif
    //Hash to compute the key (location) where let_it_go register value will be stored.
    Hash<bit<32>>(HashAlgorithm_t.CRC32) hash_1;
#ifdef CASE_FIX_3
    Hash<bit<32>>(HashAlgorithm_t.CRC32) hash_2;
#endif

    // Forwarding table and action
    action route(PortId_t port) {
        ig_tm_md.ucast_egress_port = port;
        ig_dprsr_md.drop_ctl = 0;
        }
    action nop() {}

    action drop() {
        ig_dprsr_md.drop_ctl = 0x1; // Drop packet.
    }

    action do_recirculate(){
        ig_tm_md.ucast_egress_port = 68;
    }

    table forward {
        key = {
            hdr.ipv4.dst_addr : lpm;
        }

        actions = {
            route;
            drop;
            nop;
        }
        const default_action = drop;
        size = 1024;
    }

    // FSM table and action
    action update_state(bit<16> new_state, bit<8> is_new_state_a_final_state){
        hdr.recir.pattern_state_machine_state = new_state;
        accept_state_flag = is_new_state_a_final_state;
    }
    action reset_state(){
        hdr.recir.pattern_state_machine_state = 0;
    }

    table fsm {
        key = {
            hdr.recir.pattern_state_machine_state:exact;
            hdr.app.byte: exact;
        }

        actions = {
            update_state;
            reset_state;
            nop;
        }
        const default_action = reset_state();
        size = 1024;
    }


   action original_tcp_packet_action(){
        // A. Mirror
        ig_dprsr_md.mirror_type = MIRROR_TYPE_I2E;
        // B. Recirculate
        do_recirculate();
        // C. Validate recirculated header
        hdr.recir.setValid();
        hdr.recir.packet_state=0;
        hdr.recir.pattern_state_machine_state=0;
        hdr.recir.let_it_go_register_value = RECIRCULATE;

        // D. Set TCP PORT to RECIRCULAR_PORT
        // Store the TCP port value in port register
        hdr.recir.port_value = hdr.tcp.dst_port;
        hdr.tcp.dst_port = 5555;
    }

   action original_udp_packet_action(){
        // A. Mirror
        ig_dprsr_md.mirror_type = MIRROR_TYPE_I2E;
        // B. Recirculate
        do_recirculate();
        // C. Validate recirculated header
        hdr.recir.setValid();
        hdr.recir.packet_state=0;
        hdr.recir.pattern_state_machine_state=0;
        hdr.recir.let_it_go_register_value = RECIRCULATE;

        // D. Set UDP PORT to RECIRCULAR_PORT
        // Store the UDP port value in port register
        hdr.recir.port_value = hdr.udp.src_port;
        hdr.udp.src_port = 5555;
    }

    action original_recirculated_tcp_packet_recirculate_action(){
        //Check result of the clone packet from let_it_go_register.
#ifdef CASE_FIX_1
        hdr.recir.let_it_go_register_value = let_it_go_read.execute(let_it_go_register_key);
#else
        hdr.recir.let_it_go_register_value = let_it_go_register.read(let_it_go_register_key);
#endif
        do_recirculate();
    }

    action original_recirculated_tcp_packet_accept_action(){
        hdr.tcp.dst_port = hdr.recir.port_value;
        hdr.recir.setInvalid();
    }

    action original_recirculated_tcp_packet_reject_action(){
        drop();
    }


    action original_recirculated_udp_packet_recirculate_action(){
        // Check result of the clone packet from let_it_go_register.
#ifdef CASE_FIX_1
        hdr.recir.let_it_go_register_value = let_it_go_read.execute(let_it_go_register_key);
#else
        hdr.recir.let_it_go_register_value = let_it_go_register.read(let_it_go_register_key);
#endif
        do_recirculate();
    }

    action original_recirculated_udp_packet_accept_action(){
        hdr.udp.src_port = hdr.recir.port_value;
        hdr.recir.setInvalid();
    }

    action original_recirculated_udp_packet_reject_action(){
        drop();
    }

    action recirculated_mirrored_tcp_packet_recirculate_action(){
        // If app is valid then check FSM table for pattern matching at app layer
        fsm_flag = 1;
    }

    action recirculated_mirrored_tcp_packet_accept_action(){
#ifdef CASE_FIX_1
        let_it_go_accept.execute(let_it_go_register_key);
#else
        let_it_go_register.write(let_it_go_register_key,ACCEPT);
#endif
        drop();
    }

    action recirculated_mirrored_tcp_packet_reject_action(){
#ifdef CASE_FIX_1
        let_it_go_reject.execute(let_it_go_register_key);
#else
        let_it_go_register.write(let_it_go_register_key,REJECT);
#endif
        drop();
    }


    action recirculated_mirrored_udp_packet_recirculate_action(){
        //If app is valid then check FSM table for pattern matching at app layer
        fsm_flag = 1;
    }


    action recirculated_mirrored_udp_packet_accept_action(){
#ifdef CASE_FIX_1
        let_it_go_accept.execute(let_it_go_register_key);
#else
        let_it_go_register.write(let_it_go_register_key,ACCEPT);
#endif
        drop();
    }

    action recirculated_mirrored_udp_packet_reject_action(){
#ifdef CASE_FIX_1
        let_it_go_reject.execute(let_it_go_register_key);
#else
        let_it_go_register.write(let_it_go_register_key,REJECT);
#endif
        drop();
    }

    table first_level_table {
        key = {
            /*																					hdr.udp.isValid()	hdr.tcp.isValid()	hdr.app.isValid()	hdr.recir.isValid()	hdr.recir.packet_state:ternary	hdr.recir.let_it_go_register_value:ternary	Action

	Original TCP:																false	true	true	false	_	_	original_tcp_packet_action;

	Original UDP:																true	false	true	false	_	_	original_udp_packet_action;



	Original TCP recirculated: (RECIRCULATE)						false	true	true	true	1	RECIRCULATE	original_recirculated_tcp_packet_recirculate_action;

	Original TCP recirculated: (ACCEPT)								false	true	true	true	1	ACCEPT	original_recirculated_tcp_packet_accept_action;

	Original TCP recirculated: (REJECT)								false	true	true	true	1	REJECT	original_recirculated_tcp_packet_reject_action;



	Original UDP recirculated: (RECIRCULATE)						true	false	true	true	1	RECIRCULATE	original_recirculated_udp_packet_recirculate_action;

	Original UDP recirculated: (ACCEPT)								true	false	true	true	1	ACCEPT	original_recirculated_udp_packet_accept_action;

	Original UDP recirculated: (REJECT)								true	false	true	true	1	REJECT	original_recirculated_udp_packet_reject_action;



	Recirculated Mirrored TCP : (RECIRCULATE)					false	true	true	true	10	RECIRCULATE	recirculated_mirrored_tcp_packet_recirculate_action;

	Recirculated Mirrored TCP: (ACCEPT)							false	true	false	true	10	RECIRCULATE	recirculated_mirrored_tcp_packet_accept_action;

	Recirculated Mirrored TCP: (REJECT with app header)		false	true	true	true	10	REJECT	recirculated_mirrored_tcp_packet_reject_action;

	Recirculated Mirrored TCP: (REJECT without app header)	false	true	false	true	10	REJECT	recirculated_mirrored_tcp_packet_reject_action;



	Recirculated Mirrored UDP : (RECIRCULATE)					true	false	true	true	10	RECIRCULATE	recirculated_mirrored_udp_packet_recirculate_action;

	Recirculated Mirrored UDP: (ACCEPT)							true	false	false	true	10	RECIRCULATE	recirculated_mirrored_udp_packet_accept_action;

	Recirculated Mirrored UDP: (REJECT with app header)		true	false	true	true	10	REJECT	recirculated_mirrored_udp_packet_reject_action;

	Recirculated Mirrored UDP: (REJECT without app header)true	false	false	true	10	REJECT	recirculated_mirrored_udp_packet_reject_action;

*/
            hdr.udp.isValid():exact;
            hdr.tcp.isValid():exact;
            hdr.app.isValid():exact;
            hdr.recir.isValid():exact;
            hdr.recir.packet_state:ternary;
            hdr.recir.let_it_go_register_value:ternary;
        }
        actions = {
            original_tcp_packet_action;
            original_udp_packet_action;

            original_recirculated_tcp_packet_recirculate_action;
            original_recirculated_tcp_packet_accept_action;
            original_recirculated_tcp_packet_reject_action;

            original_recirculated_udp_packet_recirculate_action;
            original_recirculated_udp_packet_accept_action;
            original_recirculated_udp_packet_reject_action;

            recirculated_mirrored_tcp_packet_recirculate_action;
            recirculated_mirrored_tcp_packet_accept_action;
            recirculated_mirrored_tcp_packet_reject_action;

            recirculated_mirrored_udp_packet_recirculate_action;
            recirculated_mirrored_udp_packet_accept_action;
            recirculated_mirrored_udp_packet_reject_action;
            nop;
        }

        const entries = {
            (false, true, true, false, _, _) :
            original_tcp_packet_action();
            (true, false, true, false, _, _) :
            original_udp_packet_action();
            (false, true, true, true, 1, RECIRCULATE) :
            original_recirculated_tcp_packet_recirculate_action();
            (false, true, true, true, 1, ACCEPT) :
            original_recirculated_tcp_packet_accept_action();
            (false, true, true, true, 1, REJECT) :
            original_recirculated_tcp_packet_reject_action();
            (true, false, true, true, 1, RECIRCULATE) :
            original_recirculated_udp_packet_recirculate_action();
            (true, false, true, true, 1, ACCEPT) :
            original_recirculated_udp_packet_accept_action();
            (true, false, true, true, 1, REJECT) :
            original_recirculated_udp_packet_reject_action();
            (false, true, true, true, 10, RECIRCULATE) :
            recirculated_mirrored_tcp_packet_recirculate_action();
            (false, true, false, true, 10, RECIRCULATE) :
            recirculated_mirrored_tcp_packet_accept_action();
            (false, true, true, true, 10, REJECT) :
            recirculated_mirrored_tcp_packet_reject_action();
            (false, true, false, true, 10, REJECT) :
            recirculated_mirrored_tcp_packet_reject_action();

            (true, false, true, true, 10, RECIRCULATE) :
            recirculated_mirrored_udp_packet_recirculate_action();
            (true, false, false, true, 10, RECIRCULATE) :
            recirculated_mirrored_udp_packet_accept_action();
            (true, false, true, true, 10, REJECT) :
            recirculated_mirrored_udp_packet_reject_action();
            (true, false, false, true, 10, REJECT) :
            recirculated_mirrored_udp_packet_reject_action();
        }

        const default_action = nop();
        size = 1024;
    }

    apply {
        if (hdr.ipv4.isValid()) {
            forward.apply();

            if(hdr.tcp.isValid()){
                // Find Register index/key/location calculation to store the let_it_go flag (Hash)
                let_it_go_register_key = hash_1.get(
                    {
                        hdr.ipv4.src_addr,
                        hdr.ipv4.dst_addr,
                        hdr.tcp.src_port,
                        hdr.tcp.dst_port,
                        hdr.ipv4.total_len,
                        hdr.ipv4.identification
                    })[31:0];
            }
            else if(hdr.udp.isValid()){
                // Find Register index/key/location calculation to store the let_it_go flag (Hash)
#ifdef CASE_FIX_3
                let_it_go_register_key = hash_2.get(
#else
                let_it_go_register_key = hash_1.get(
#endif
                    {
                        hdr.ipv4.src_addr,
                        hdr.ipv4.dst_addr,
                        hdr.udp.src_port,
                        hdr.udp.dst_port
                    })[31:0];
            }

            first_level_table.apply();

            if (fsm_flag == 1){
                fsm.apply();
                hdr.recir.let_it_go_register_value = accept_state_flag;
                do_recirculate();
                hdr.recir.setInvalid();
            }
        }
    }
}
# 12 "tna_fsm_dpi.p4" 2



    /***********************  P A R S E R  **************************/

parser TofinoEgressParser(
    packet_in pkt,
    out egress_intrinsic_metadata_t eg_intr_md)
{
    state start {
        pkt.extract(eg_intr_md);
        transition accept;
    }
}


parser EgressParser(
        packet_in pkt,
    /* User */
    out egress_headers_t hdr,
    out egress_metadata_t eg_md,
    /* Intrinsic */
    out egress_intrinsic_metadata_t eg_intr_md)
    {
    Checksum() ipv4_checksum;
    TofinoEgressParser() tofino_parser;

    state start {
        tofino_parser.apply(pkt, eg_intr_md);
        transition parse_metadata;
    }

    state parse_metadata {
        mirror_h mirror_md = pkt.lookahead<mirror_h>();
        transition select(mirror_md.pkt_type) {
            PKT_TYPE_MIRROR : parse_mirror_tagging_state;
            default : parse_normal_tagging_state;
        }
    }

    state parse_normal_tagging_state {
        eg_md.packet_state = 1;
        transition parse_ethernet;
    }

    state parse_mirror_tagging_state {
        mirror_h mirror_md;
        pkt.extract(mirror_md);
        eg_md.packet_state = 10;
        transition parse_ethernet;
    }

    state parse_ethernet {
        pkt.extract(hdr.ethernet);
        transition select(hdr.ethernet.ether_type) {
            ETHERTYPE_IPV4 : parse_ipv4;
            ETHERTYPE_IPV6 : parse_ipv6;
            default : accept;
        }
    }

//ToDo: add ipv4.len field to move further only if ipv4.len >100 else accept

    state parse_ipv4 {
        pkt.extract(hdr.ipv4);
        ipv4_checksum.add(hdr.ipv4);
        transition select(hdr.ipv4.total_len) {
            0 .. 100 : accept;
            100 .. 65535 : parse_l4_temp_state;
               default : accept;
        }
    }

    state parse_l4_temp_state {
        transition select(hdr.ipv4.protocol) {
            IP_PROTOCOLS_TCP : parse_tcp;
            IP_PROTOCOLS_UDP : parse_udp;
            default : accept;
        }
    }

    state parse_ipv6 {
        pkt.extract(hdr.ipv6);
        transition select(hdr.ipv6.next_hdr) {
            IP_PROTOCOLS_TCP : parse_tcp;
            IP_PROTOCOLS_UDP : parse_udp;
            default : accept;
        }
    }

    state parse_udp {
        pkt.extract(hdr.udp);
        transition select(hdr.udp.src_port) {
            DNS: parse_app;
            RECIRCULAR_PORT: parse_recirculation;
            default: accept;
        }
    }

    state parse_tcp {
        pkt.extract(hdr.tcp);
        transition select(hdr.tcp.dst_port) {
            HTTP: parse_app;
            HTTPS: parse_app;
            RECIRCULAR_PORT: parse_recirculation;
            default: accept;
        }
    }

    state parse_recirculation {
        pkt.extract(hdr.recir);
        transition parse_app;
    }

    state parse_app {
        pkt.extract(hdr.app);
        transition accept;
    }

}

    /***************** M A T C H - A C T I O N  *********************/

control Egress(
    /* User */
    inout egress_headers_t hdr,
    inout egress_metadata_t eg_md,
    /* Intrinsic */
    in egress_intrinsic_metadata_t eg_intr_md,
    in egress_intrinsic_metadata_from_parser_t eg_prsr_md,
    inout egress_intrinsic_metadata_for_deparser_t eg_dprsr_md,
    inout egress_intrinsic_metadata_for_output_port_t eg_oport_md)
    {

    apply {
        // A. Normal packet tagging
        if(hdr.recir.packet_state == 0 && eg_md.packet_state == 1){
        hdr.recir.packet_state = 1;
        }

        // B. Cloned packet tagging
        if(hdr.recir.packet_state == 0 && eg_md.packet_state == 10){
        hdr.recir.packet_state = 10;
        }
    }
}

    /*********************  D E P A R S E R  ************************/

control EgressDeparser(
        packet_out pkt,
    /* User */
    inout egress_headers_t hdr,
    in egress_metadata_t eg_md,
    /* Intrinsic */
    in egress_intrinsic_metadata_for_deparser_t eg_dprsr_md)
    {
    apply {
        pkt.emit(hdr.ethernet);
        pkt.emit(hdr.ipv4);
        pkt.emit(hdr.ipv6);
        pkt.emit(hdr.udp);
        pkt.emit(hdr.tcp);
        pkt.emit(hdr.recir);
        pkt.emit(hdr.app);
    }
}
# 13 "tna_fsm_dpi.p4" 2

Pipeline(
    IngressParser(),
    Ingress(),
    IngressDeparser(),
    EgressParser(),
    Egress(),
    EgressDeparser()
) pipe;

Switch(pipe) main;
