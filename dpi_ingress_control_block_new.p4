//SwitchIngress code

control Ingress(
    /* User */
    inout ingress_headers_t                       hdr,
    inout ingress_metadata_t                      meta,
    /* Intrinsic */
    in    ingress_intrinsic_metadata_t               ig_intr_md,
    in    ingress_intrinsic_metadata_from_parser_t   ig_prsr_md,
    inout ingress_intrinsic_metadata_for_deparser_t  ig_dprsr_md,
    inout ingress_intrinsic_metadata_for_tm_t        ig_tm_md)
{
// Declare register that will pass results from cloned analyzed packet to original recirculated packet
// temparary variable to store value.
bit<32> let_it_go_register_key=0;
bit<8> let_it_go_register_value=0;
bit<16> port_value=0;
bit<16> accept_state_flag=0;
bit<2> fsm_flag=0;

//Registers
    Register<bit<8>, bit<32>>(400000000) let_it_go_register;
    Register<bit<16>, bit<32>>(400000000) port_register;
/*
    RegisterAction<bit<4>, bit<32>, bit<4>>(let_it_go_register) let_it_go_register_action = {
        void apply(inout bit<4> val, out bit<4> rv) {
            rv = val;
            val = let_it_go_register_value;
        }
    };

    let_it_go_register_value = CLONED_RECIRCULATED_PACKET_FAIL;
    let_it_go_register_action.execute(let_it_go_register_key);
*/

//Hash to compute the key (location) where let_it_go register value will be stored.
Hash<bit<32>>(HashAlgorithm_t.CRC32) hash_1;
//Hash<bit<32>>(HashAlgorithm_t.CRC32) hash_2;
// Forwarding table and action
    action route(PortId_t port, bit<48> smac, bit<48> dmac) {
        ig_tm_md.ucast_egress_port = port;
        //hdr.ethernet.src_addr = smac;
        //hdr.ethernet.dst_addr = dmac;
        //hdr.ipv4.ttl = hdr.ipv4.ttl - 1;
        ig_dprsr_md.drop_ctl = 0;
        }
    action nop() {}

    action drop() {
        ig_dprsr_md.drop_ctl = 0x1; // Drop packet.
    }

    action do_recirculate(){
    ig_tm_md.ucast_egress_port = 68;
    }

    table forward {
        key = {
            hdr.ipv4.dst_addr : lpm;
        }

        actions = {
            route;
            drop;
            nop;
        }
        const default_action = drop;
        size = 1024;
    }

// FSM table and action
    action update_state(bit<16> new_state, bit<16> is_new_state_a_final_state){
    	hdr.recir.pattern_state_machine_state = new_state;
        accept_state_flag = is_new_state_a_final_state;
    }
    action reset_state(){
    	hdr.recir.pattern_state_machine_state = 0;
    }

    table fsm {
        key = {
	     hdr.recir.pattern_state_machine_state:exact;
            hdr.app.byte: exact;
        }

        actions = {
            update_state;
	        reset_state;
            nop;
        }
        const default_action = reset_state();
        size = 1024;
    }


			action original_tcp_packet_action(){
      // Find Register index/key/location calculation to store the let_it_go flag (Hash)
			let_it_go_register_key = hash_1.get(
			{
				hdr.ipv4.src_addr,
				hdr.ipv4.dst_addr,
				hdr.tcp.src_port,
				hdr.tcp.dst_port,
				hdr.ipv4.total_len,
				hdr.ipv4.identification
				})[31:0];

        // A. Mirror
            ig_dprsr_md.mirror_type = MIRROR_TYPE_I2E;
        // B. Recirculate
            do_recirculate();
        // C. Validate recirculated header
            hdr.recir.setValid();
            hdr.recir.packet_state=0;
            hdr.recir.pattern_state_machine_state=0;

        // D. Set TCP PORT to RECIRCULAR_PORT
        //    Store the TCP port value in port register
            port_register.write(let_it_go_register_key, hdr.tcp.dst_port);
            hdr.tcp.dst_port = 5555;

        // E. Put RECIRCULATE state in let_it_go_register
            let_it_go_register.write(let_it_go_register_key,RECIRCULATE);
			}

			action original_udp_packet_action(){
      // Find Register index/key/location calculation to store the let_it_go flag (Hash)
			let_it_go_register_key = hash_1.get(
			{
			hdr.ipv4.src_addr,
            hdr.ipv4.dst_addr,
            hdr.udp.src_port,
            hdr.udp.dst_port})[31:0];

       // A. Mirror
            ig_dprsr_md.mirror_type = MIRROR_TYPE_I2E;
        // B. Recirculate
            do_recirculate();
        // C. Validate recirculated header
            hdr.recir.setValid();
            hdr.recir.packet_state=0;
            hdr.recir.pattern_state_machine_state=0;

        // D. Set UDP PORT to RECIRCULAR_PORT
        //    Store the UDP port value in port register
            port_register.write(let_it_go_register_key, hdr.udp.src_port);
            hdr.udp.src_port = 5555;

        // E. Put RECIRCULATE state in let_it_go_register
            let_it_go_register.write(let_it_go_register_key,RECIRCULATE);
			}

			action original_recirculated_tcp_packet_action(){
       // Find Register index/key/location calculation to store the let_it_go flag (Hash)
			let_it_go_register_key = hash_1.get(
			{
				hdr.ipv4.src_addr,
				hdr.ipv4.dst_addr,
				hdr.tcp.src_port,
				hdr.tcp.dst_port,
				hdr.ipv4.total_len,
				hdr.ipv4.identification
				})[31:0];

        // A. Check result of the clone packet from let_it_go_register.
           let_it_go_register_value = let_it_go_register.read(let_it_go_register_key);

        // B. CASE 1: ACCEPT | Remove the recir header
        //    Restore the TCP/UDP port value as well
        /*    if (let_it_go_register_value == ACCEPT){
            hdr.recir.setInvalid();
            port_value = port_register.read(let_it_go_register_key);
            hdr.tcp.dst_port = port_value;
            }*/
            hdr.recir.let_it_go_register_value = ACCEPT;

        // C. CASE 2: REJECT | drop the packet
            if (let_it_go_register_value == REJECT){
            drop();
            }
        // D. CASE 3: RECIRCULATE | recirculate the packet
            if (let_it_go_register_value == RECIRCULATE){
            do_recirculate();
            }
			}

            action original_recirculated_udp_packet_action(){
     // Find Register index/key/location calculation to store the let_it_go flag (Hash)
			let_it_go_register_key = hash_1.get(
			{
			hdr.ipv4.src_addr,
            hdr.ipv4.dst_addr,
            hdr.udp.src_port,
            hdr.udp.dst_port})[31:0];

        // A. Check result of the clone packet from let_it_go_register.
           let_it_go_register_value = let_it_go_register.read(let_it_go_register_key);

        // B. CASE 1: ACCEPT | Remove the recir header
        //    Restore the TCP/UDP port value as well
         /*   if (let_it_go_register_value == ACCEPT){
            hdr.recir.setInvalid();
            port_value = port_register.read(let_it_go_register_key);
            hdr.udp.src_port = port_value;
            }*/
            hdr.recir.let_it_go_register_value = ACCEPT;

        // C. CASE 2: REJECT | drop the packet
            if (let_it_go_register_value == REJECT){
            drop();
            }
        // D. CASE 3: RECIRCULATE | recirculate the packet
            if (let_it_go_register_value == RECIRCULATE){
            do_recirculate();
            }
			}

            action recirculated_mirrored_tcp_not_consumed_packet_action(){
      // Find Register index/key/location calculation to store the let_it_go flag (Hash)
			let_it_go_register_key = hash_1.get(
			{
				hdr.ipv4.src_addr,
				hdr.ipv4.dst_addr,
				hdr.tcp.src_port,
				hdr.tcp.dst_port,
				hdr.ipv4.total_len,
				hdr.ipv4.identification
				})[31:0];

        // B. If app is valid then check FSM table for pattern matching at app layer
           //fsm.apply();
           fsm_flag = 1;
        // C. If pattern is found then push REJECT state in let_it_go register
         /*  if(accept_state_flag == 1){
             let_it_go_register.write(let_it_go_register_key,REJECT);
             drop();
           }
		   else{
		   do_recirculate();
		   }*/
		   hdr.recir.accept_state_flag = 1;

        // E. Truncate the app header
            hdr.app.setInvalid();
			}

            action recirculated_mirrored_tcp_consumed_packet_action(){
      // Find Register index/key/location calculation to store the let_it_go flag (Hash)
			let_it_go_register_key = hash_1.get(
			{
				hdr.ipv4.src_addr,
				hdr.ipv4.dst_addr,
				hdr.tcp.src_port,
				hdr.tcp.dst_port,
				hdr.ipv4.total_len,
				hdr.ipv4.identification
				})[31:0];
            let_it_go_register.write(let_it_go_register_key,ACCEPT);
			drop();
			}



            action recirculated_mirrored_udp_not_consumed_packet_action(){
		// Find Register index/key/location calculation to store the let_it_go flag (Hash)
			let_it_go_register_key = hash_1.get(
			{
			hdr.ipv4.src_addr,
            hdr.ipv4.dst_addr,
            hdr.udp.src_port,
            hdr.udp.dst_port})[31:0];


        // B. If app is valid then check FSM table for pattern matching at app layer
           //fsm.apply();
            fsm_flag = 1;
        // C. If pattern is found then push REJECT state in let_it_go register
           /*if(accept_state_flag == 1){
             let_it_go_register.write(let_it_go_register_key,REJECT);
             drop();
           }
		   else{
            do_recirculate();
		   }*/
        hdr.recir.accept_state_flag = 1;
			}

            action recirculated_mirrored_udp_consumed_packet_action(){
      // Find Register index/key/location calculation to store the let_it_go flag (Hash)
			let_it_go_register_key = hash_1.get(
			{
			hdr.ipv4.src_addr,
            hdr.ipv4.dst_addr,
            hdr.udp.src_port,
            hdr.udp.dst_port})[31:0];

        // A. Check if app is consumed and let_it_go is in RECIRCULATE state
             let_it_go_register.write(let_it_go_register_key,ACCEPT);
             drop();
			}

    table first_level_table {
        key = {
		//Original TCP:
		// Condition: 		hdr.udp.isValid() == FALSE		hdr.tcp.isValid() == TRUE		hdr.app.isValid() = TRUE		hdr.recir.isValid()=FALSE		hdr.recir.packet_state = don't care
		//Original UDP:
		// Condition: 		hdr.udp.isValid() == TRUE		hdr.tcp.isValid() == FALSE		hdr.app.isValid() = TRUE		hdr.recir.isValid()=FALSE		hdr.recir.packet_state = don't care
		// Original TCP recirculated:
		// Condition: 		hdr.udp.isValid() == FALSE		hdr.tcp.isValid() == TRUE		hdr.app.isValid() = TRUE		hdr.recir.isValid()=TRUE		hdr.recir.packet_state = 1
		// Original UDP recirculated:
		// Condition: 		hdr.udp.isValid() == TRUE		hdr.tcp.isValid() == FALSE		hdr.app.isValid() = TRUE		hdr.recir.isValid()=TRUE		hdr.recir.packet_state = 1
		// Recirculated Mirrored TCP not consumed packet:
		// Condition: 		hdr.udp.isValid() == FALSE		hdr.tcp.isValid() == TRUE		hdr.app.isValid() = TRUE		hdr.recir.isValid()=TRUE		hdr.recir.packet_state = 10
		// Recirculated Mirrored TCP consumed packet
		// Condition: 		hdr.udp.isValid() == FALSE		hdr.tcp.isValid() == TRUE		hdr.app.isValid() = FALSE		hdr.recir.isValid()=TRUE		hdr.recir.packet_state = 10
		// Recirculated Mirrored UDP not consumed packet
		// Condition: 		hdr.udp.isValid() == TRUE		hdr.tcp.isValid() == FALSE		hdr.app.isValid() = TRUE		hdr.recir.isValid()=TRUE		hdr.recir.packet_state = 10
		// Recirculated Mirrored UDP consumed packet
		// Condition: 		hdr.udp.isValid() == TRUE		hdr.tcp.isValid() == FALSE		hdr.app.isValid() = FALSE		hdr.recir.isValid()=TRUE		hdr.recir.packet_state = 10
		hdr.udp.isValid():exact;
		hdr.tcp.isValid():exact;
		hdr.app.isValid():exact;
		hdr.recir.isValid():exact;
		hdr.recir.packet_state:ternary;
        }
        actions = {
            original_tcp_packet_action;
            original_udp_packet_action;
            original_recirculated_tcp_packet_action;
            original_recirculated_udp_packet_action;
            recirculated_mirrored_tcp_not_consumed_packet_action;
            recirculated_mirrored_tcp_consumed_packet_action;
            recirculated_mirrored_udp_not_consumed_packet_action;
            recirculated_mirrored_udp_consumed_packet_action;
            nop;
        }
        const default_action = nop();
        size = 1024;
    }

    action  original_tcp_port_value(){
            hdr.recir.setInvalid();
            port_value = port_register.read(let_it_go_register_key);
            hdr.tcp.dst_port = port_value;
    }
     action original_udp_port_value(){
            hdr.recir.setInvalid();
            port_value = port_register.read(let_it_go_register_key);
            hdr.udp.src_port = port_value;
     }
     action pattern_found_update(){
            let_it_go_register.write(let_it_go_register_key,REJECT);
             drop();
     }


    table second_level_table {
        key = {
		// Original TCP recirculated let_it_go = ACCEPT(if let_it_go / port register updated):
		// Condition: 		hdr.udp.isValid() == FALSE		hdr.tcp.isValid() == TRUE		hdr.app.isValid() = TRUE		hdr.recir.isValid()=TRUE		hdr.recir.packet_state = 1 hdr.recir.let_it_go_register_value = ACCEPT  hdr.recir.accept_state_flag = 0

		// Original UDP recirculated let_it_go = ACCEPT (if let_it_go / port register updated):
		// Condition: 		hdr.udp.isValid() == TRUE		hdr.tcp.isValid() == FALSE		hdr.app.isValid() = TRUE		hdr.recir.isValid()=TRUE		hdr.recir.packet_state = 1 hdr.recir.let_it_go_register_value = ACCEPT  hdr.recir.accept_state_flag = 0

		// Recirculated Mirrored TCP not consumed packet accepted flag 1 (if accepted_flag / let it go register update):
		// Condition: 		hdr.udp.isValid() == FALSE		hdr.tcp.isValid() == TRUE		hdr.app.isValid() = TRUE		hdr.recir.isValid()=TRUE		hdr.recir.packet_state = 10 hdr.recir.let_it_go_register_value = 0  hdr.recir.accept_state_flag = 1

		// Recirculated Mirrored TCP not consumed packet accepted flag 0 (if accepted_flag / let it go register update):
		// Condition: 		hdr.udp.isValid() == FALSE		hdr.tcp.isValid() == TRUE		hdr.app.isValid() = TRUE		hdr.recir.isValid()=TRUE		hdr.recir.packet_state = 10 hdr.recir.let_it_go_register_value = 0  hdr.recir.accept_state_flag = 0

		// Recirculated Mirrored UDP not consumed packet accepted flag 1 (if accepted_flag / let it go register update):
		// Condition: 		hdr.udp.isValid() == TRUE		hdr.tcp.isValid() == FALSE		hdr.app.isValid() = TRUE		hdr.recir.isValid()=TRUE		hdr.recir.packet_state = 10 hdr.recir.let_it_go_register_value = 0  hdr.recir.accept_state_flag = 1

		// Recirculated Mirrored UDP not consumed packet accepted flag 0 (if accepted_flag / let it go register update):
		// Condition: 		hdr.udp.isValid() == TRUE		hdr.tcp.isValid() == FALSE		hdr.app.isValid() = TRUE		hdr.recir.isValid()=TRUE		hdr.recir.packet_state = 10 hdr.recir.let_it_go_register_value = 0  hdr.recir.accept_state_flag = 1

		hdr.udp.isValid():exact;
		hdr.tcp.isValid():exact;
		hdr.app.isValid():exact;
		hdr.recir.isValid():exact;
		hdr.recir.packet_state:ternary;
		hdr.recir.let_it_go_register_value:exact;
		hdr.recir.accept_state_flag:exact;

        }
        actions = {
            original_tcp_port_value;
            original_udp_port_value;
            pattern_found_update;
            do_recirculate;
            nop;
        }
        const default_action = nop();
        size = 1024;
    }

    apply {
        if (hdr.ipv4.isValid()) {
            forward.apply();
			first_level_table.apply();
	        if (fsm_flag == 1){
            fsm.apply();
            }
            second_level_table.apply();

        }

    }
}
