#!/bin/bash
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
# Array of top n url number list
declare -a top_dn_number=(1 2 3 4 5 6 7 8 9 10 20 30 40 50 60 70 80 90 100 200 300 400 500 600 700 800 900 1000 2000 3000 4000 5000 6000 7000 8000 9000 10000)

# 1. Read top n urls from a given list.
IFS=$'\n' read -d '' -r -a websites < $PWD/alexa.txt ; #echo ${websites[*]}

for i in "${top_dn_number[@]}"
do
top_dn_index_end=$i;
top_dn_number_idx="${websites[@]:0:$((top_dn_index_end))}"
echo "top alexa DN no: ", $top_dn_index_end
sleep 1
# 2. replace each url string's "." symbol with "@".
# 3. create list of first_n_url_list = [[url1 | url2| urln].
top_dn_number_idx="${top_dn_number_idx// /|}"
top_dn_number_idx="${top_dn_number_idx//./@}"
echo $top_dn_number_idx

# 4. Send composite string to DFA converter python process.
python3 regextodfa.py $top_dn_number_idx $top_dn_index_end


# 5. sleep 1
sleep 1
# 6. pkill python3
pkill python3
# 7. sleep 1
sleep 1
# Loop end
#exit 0
done
