# either use nffw_server or rswitch_server NI to send packets
# Obtain time from system in time start variable
import time
from scapy.all import *
import random
import string
from random import randint

start_time = time.time()
# Create a TCP packet with raw() function on top of TCP header to compensate the packet_length variable
#Sample command: python3 Custom_payload_tcp.py 500 10.0.0.2 80 15 veth0 google.com
#packet_length = 500
packet_length = int(sys.argv[1])
payload = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(packet_length- 14-20-20))

#packet = IP(dst="10.0.0.2")/TCP(dport=80)/payload
packet = IP(dst=sys.argv[2])/TCP(dport=int(sys.argv[3]))/payload
#Get the time end variable
end_time = time.time()
i=0
while end_time-start_time < int(sys.argv[4]): # 60 second value passed as 60 as CLA
    #send(packet,iface="veth0")
    pos = randint(0, len(payload) - 1)
    print(f'URL at {pos} in payload of {len(payload)}')
    packet = IP(dst=sys.argv[2])/TCP(dport=int(sys.argv[3]),sport=i)/"".join((payload[:pos], sys.argv[6], payload[pos:]))
    send(packet,iface=sys.argv[5])
    end_time = time.time()
    i=i+1


