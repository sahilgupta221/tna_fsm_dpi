from netaddr import IPAddress

p4 = bfrt.tna_fsm_dpi.pipe

# This function can clear all the tables and later on other fixed objects
# once bfrt support is added.
def clear_all(verbose=True, batching=True):
    global p4
    global bfrt

    # The order is important. We do want to clear from the top, i.e.
    # delete objects that use other objects, e.g. table entries use
    # selector groups and selector groups use action profile members

    for table_types in (['MATCH_DIRECT', 'MATCH_INDIRECT_SELECTOR'],
                        ['SELECTOR'],
                        ['ACTION_PROFILE']):
        for table in p4.info(return_info=True, print_info=False):
            if table['type'] in table_types:
                if verbose:
                    print("Clearing table {:<40} ... ".
                          format(table['full_name']), end='', flush=True)
                table['node'].clear(batch=batching)
                if verbose:
                    print('Done')

clear_all(verbose=True)

# Update forward table for routing/forwarding
forward_table = p4.Ingress.forward
forward_table.add_with_route(dst_addr=IPAddress('10.0.0.2'),dst_addr_p_length=32,port=2)
forward_table.add_with_route(dst_addr=IPAddress('10.0.0.1'),dst_addr_p_length=32,port=1)

# Update HTTP TCAM table
fsm = p4.Ingress.fsm
fsm.add_with_update_state(pattern_state_machine_state= 0,byte=0x33,new_state= 1,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 0,byte=0x61,new_state= 2,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 0,byte=0x62,new_state= 3,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 0,byte=0x67,new_state= 4,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 0,byte=0x66,new_state= 5,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 0,byte=0x6a,new_state= 6,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 0,byte=0x71,new_state= 7,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 0,byte=0x73,new_state= 8,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 0,byte=0x74,new_state= 9,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 0,byte=0x77,new_state= 10,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 0,byte=0x79,new_state= 11,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 0,byte=0x7a,new_state= 12,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 1,byte=0x36,new_state= 130,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 2,byte=0x6d,new_state= 121,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 3,byte=0x61,new_state= 113,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 4,byte=0x6f,new_state= 104,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 5,byte=0x61,new_state= 93,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 6,byte=0x64,new_state= 25,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 7,byte=0x71,new_state= 25,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 8,byte=0x69,new_state= 66,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 8,byte=0x6f,new_state= 67,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 9,byte=0x61,new_state= 49,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 9,byte=0x6d,new_state= 50,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 10,byte=0x69,new_state= 37,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 11,byte=0x61,new_state= 19,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 11,byte=0x6f,new_state= 20,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 12,byte=0x6f,new_state= 13,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 13,byte=0x6f,new_state= 14,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 14,byte=0x6d,new_state= 15,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 15,byte=0x2e,new_state= 16,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 16,byte=0x75,new_state= 17,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 17,byte=0x73,new_state= 18,is_new_state_a_final_state=1)
fsm.add_with_update_state(pattern_state_machine_state= 19,byte=0x68,new_state= 30,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 20,byte=0x75,new_state= 21,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 21,byte=0x74,new_state= 22,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 22,byte=0x75,new_state= 23,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 23,byte=0x62,new_state= 24,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 24,byte=0x65,new_state= 25,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 25,byte=0x2e,new_state= 26,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 26,byte=0x63,new_state= 27,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 27,byte=0x6f,new_state= 28,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 28,byte=0x6d,new_state= 18,is_new_state_a_final_state=1)
fsm.add_with_update_state(pattern_state_machine_state= 30,byte=0x6f,new_state= 31,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 31,byte=0x6f,new_state= 25,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 37,byte=0x6b,new_state= 38,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 38,byte=0x69,new_state= 39,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 39,byte=0x70,new_state= 40,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 40,byte=0x65,new_state= 41,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 41,byte=0x64,new_state= 42,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 42,byte=0x69,new_state= 43,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 43,byte=0x61,new_state= 44,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 44,byte=0x2e,new_state= 45,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 45,byte=0x6f,new_state= 46,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 46,byte=0x72,new_state= 47,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 47,byte=0x67,new_state= 18,is_new_state_a_final_state=1)
fsm.add_with_update_state(pattern_state_machine_state= 49,byte=0x6f,new_state= 58,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 50,byte=0x61,new_state= 51,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 51,byte=0x6c,new_state= 52,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 52,byte=0x6c,new_state= 25,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 58,byte=0x62,new_state= 59,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 59,byte=0x61,new_state= 31,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 66,byte=0x6e,new_state= 74,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 67,byte=0x68,new_state= 68,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 68,byte=0x75,new_state= 25,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 130,byte=0x30,new_state= 79,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 74,byte=0x61,new_state= 75,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 75,byte=0x2e,new_state= 76,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 76,byte=0x63,new_state= 77,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 77,byte=0x6f,new_state= 78,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 78,byte=0x6d,new_state= 79,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 79,byte=0x2e,new_state= 80,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 80,byte=0x63,new_state= 81,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 81,byte=0x6e,new_state= 18,is_new_state_a_final_state=1)
fsm.add_with_update_state(pattern_state_machine_state= 93,byte=0x63,new_state= 94,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 94,byte=0x65,new_state= 95,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 95,byte=0x62,new_state= 96,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 96,byte=0x6f,new_state= 97,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 97,byte=0x6f,new_state= 98,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 98,byte=0x6b,new_state= 25,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 104,byte=0x6f,new_state= 105,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 105,byte=0x67,new_state= 106,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 106,byte=0x6c,new_state= 24,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 113,byte=0x69,new_state= 114,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 114,byte=0x64,new_state= 68,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 121,byte=0x61,new_state= 122,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 122,byte=0x7a,new_state= 123,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 123,byte=0x6f,new_state= 124,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 124,byte=0x6e,new_state= 25,is_new_state_a_final_state=0)


bfrt.complete_operations()

# Final programming
print("""
******************* PROGAMMING RESULTS *****************
""")

print ("Table forward table:")
forward_table.dump(table=True)

print ("Table fsm:")
fsm.dump(table=True)

