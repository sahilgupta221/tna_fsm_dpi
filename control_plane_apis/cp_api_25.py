from netaddr import IPAddress

p4 = bfrt.tna_fsm_dpi.pipe

# This function can clear all the tables and later on other fixed objects
# once bfrt support is added.
def clear_all(verbose=True, batching=True):
    global p4
    global bfrt

    # The order is important. We do want to clear from the top, i.e.
    # delete objects that use other objects, e.g. table entries use
    # selector groups and selector groups use action profile members

    for table_types in (['MATCH_DIRECT', 'MATCH_INDIRECT_SELECTOR'],
                        ['SELECTOR'],
                        ['ACTION_PROFILE']):
        for table in p4.info(return_info=True, print_info=False):
            if table['type'] in table_types:
                if verbose:
                    print("Clearing table {:<40} ... ".
                          format(table['full_name']), end='', flush=True)
                table['node'].clear(batch=batching)
                if verbose:
                    print('Done')

clear_all(verbose=True)

# Update forward table for routing/forwarding
forward_table = p4.Ingress.forward
forward_table.add_with_route(dst_addr=IPAddress('10.0.0.2'),dst_addr_p_length=32,port=2)
forward_table.add_with_route(dst_addr=IPAddress('10.0.0.1'),dst_addr_p_length=32,port=1)

# Update HTTP TCAM table
fsm = p4.Ingress.fsm
fsm.add_with_update_state(pattern_state_machine_state= 0,byte=0x33,new_state= 1,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 0,byte=0x61,new_state= 2,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 0,byte=0x62,new_state= 3,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 0,byte=0x67,new_state= 4,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 0,byte=0x66,new_state= 5,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 0,byte=0x6a,new_state= 6,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 0,byte=0x6d,new_state= 7,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 0,byte=0x6c,new_state= 8,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 0,byte=0x6f,new_state= 9,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 0,byte=0x6e,new_state= 10,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 0,byte=0x71,new_state= 11,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 0,byte=0x73,new_state= 12,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 0,byte=0x72,new_state= 13,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 0,byte=0x74,new_state= 14,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 0,byte=0x77,new_state= 15,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 0,byte=0x76,new_state= 16,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 0,byte=0x79,new_state= 17,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 0,byte=0x78,new_state= 18,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 0,byte=0x7a,new_state= 19,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 1,byte=0x36,new_state= 230,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 2,byte=0x6d,new_state= 212,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 2,byte=0x6c,new_state= 213,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 3,byte=0x61,new_state= 204,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 4,byte=0x6f,new_state= 195,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 5,byte=0x61,new_state= 184,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 6,byte=0x64,new_state= 33,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 7,byte=0x69,new_state= 155,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 7,byte=0x79,new_state= 156,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 8,byte=0x69,new_state= 148,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 9,byte=0x66,new_state= 139,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 10,byte=0x65,new_state= 129,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 11,byte=0x71,new_state= 33,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 12,byte=0x69,new_state= 107,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 12,byte=0x6f,new_state= 108,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 13,byte=0x65,new_state= 98,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 14,byte=0x61,new_state= 81,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 14,byte=0x6d,new_state= 82,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 15,byte=0x65,new_state= 61,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 15,byte=0x69,new_state= 62,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 16,byte=0x6b,new_state= 33,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 17,byte=0x61,new_state= 38,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 17,byte=0x6f,new_state= 39,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 18,byte=0x69,new_state= 26,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 19,byte=0x6f,new_state= 20,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 20,byte=0x6f,new_state= 21,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 21,byte=0x6d,new_state= 22,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 22,byte=0x2e,new_state= 23,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 23,byte=0x75,new_state= 24,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 24,byte=0x73,new_state= 25,is_new_state_a_final_state=1)
fsm.add_with_update_state(pattern_state_machine_state= 26,byte=0x6e,new_state= 27,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 27,byte=0x68,new_state= 28,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 28,byte=0x75,new_state= 29,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 29,byte=0x61,new_state= 30,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 30,byte=0x6e,new_state= 31,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 31,byte=0x65,new_state= 32,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 32,byte=0x74,new_state= 33,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 33,byte=0x2e,new_state= 34,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 34,byte=0x63,new_state= 35,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 35,byte=0x6f,new_state= 36,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 36,byte=0x6d,new_state= 25,is_new_state_a_final_state=1)
fsm.add_with_update_state(pattern_state_machine_state= 38,byte=0x68,new_state= 49,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 39,byte=0x75,new_state= 40,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 40,byte=0x74,new_state= 41,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 41,byte=0x75,new_state= 42,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 42,byte=0x62,new_state= 43,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 43,byte=0x65,new_state= 33,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 49,byte=0x6f,new_state= 50,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 50,byte=0x6f,new_state= 33,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 61,byte=0x69,new_state= 74,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 62,byte=0x6b,new_state= 63,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 63,byte=0x69,new_state= 64,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 64,byte=0x70,new_state= 65,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 65,byte=0x65,new_state= 66,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 66,byte=0x64,new_state= 67,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 67,byte=0x69,new_state= 68,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 68,byte=0x61,new_state= 69,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 69,byte=0x2e,new_state= 70,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 70,byte=0x6f,new_state= 71,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 71,byte=0x72,new_state= 72,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 72,byte=0x67,new_state= 25,is_new_state_a_final_state=1)
fsm.add_with_update_state(pattern_state_machine_state= 74,byte=0x62,new_state= 50,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 81,byte=0x6f,new_state= 90,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 82,byte=0x61,new_state= 83,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 83,byte=0x6c,new_state= 84,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 84,byte=0x6c,new_state= 33,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 90,byte=0x62,new_state= 91,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 91,byte=0x61,new_state= 50,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 98,byte=0x64,new_state= 99,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 99,byte=0x64,new_state= 100,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 100,byte=0x69,new_state= 32,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 107,byte=0x6e,new_state= 115,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 108,byte=0x68,new_state= 109,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 109,byte=0x75,new_state= 33,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 115,byte=0x61,new_state= 116,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 116,byte=0x2e,new_state= 117,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 117,byte=0x63,new_state= 118,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 118,byte=0x6f,new_state= 119,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 119,byte=0x6d,new_state= 120,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 120,byte=0x2e,new_state= 121,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 121,byte=0x63,new_state= 122,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 122,byte=0x6e,new_state= 25,is_new_state_a_final_state=1)
fsm.add_with_update_state(pattern_state_machine_state= 129,byte=0x74,new_state= 130,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 130,byte=0x66,new_state= 131,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 131,byte=0x6c,new_state= 132,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 132,byte=0x69,new_state= 133,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 133,byte=0x78,new_state= 33,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 139,byte=0x66,new_state= 140,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 140,byte=0x69,new_state= 141,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 141,byte=0x63,new_state= 43,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 148,byte=0x76,new_state= 43,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 155,byte=0x63,new_state= 168,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 156,byte=0x73,new_state= 157,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 157,byte=0x68,new_state= 158,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 158,byte=0x6f,new_state= 159,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 159,byte=0x70,new_state= 160,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 160,byte=0x69,new_state= 161,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 161,byte=0x66,new_state= 162,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 162,byte=0x79,new_state= 33,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 168,byte=0x72,new_state= 169,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 169,byte=0x6f,new_state= 170,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 170,byte=0x73,new_state= 171,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 171,byte=0x6f,new_state= 172,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 172,byte=0x66,new_state= 32,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 184,byte=0x63,new_state= 185,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 185,byte=0x65,new_state= 186,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 186,byte=0x62,new_state= 187,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 187,byte=0x6f,new_state= 188,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 188,byte=0x6f,new_state= 16,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 195,byte=0x6f,new_state= 196,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 196,byte=0x67,new_state= 197,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 197,byte=0x6c,new_state= 43,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 204,byte=0x69,new_state= 205,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 205,byte=0x64,new_state= 109,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 212,byte=0x61,new_state= 222,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 213,byte=0x69,new_state= 214,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 214,byte=0x70,new_state= 215,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 215,byte=0x61,new_state= 162,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 222,byte=0x7a,new_state= 223,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 223,byte=0x6f,new_state= 224,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 224,byte=0x6e,new_state= 33,is_new_state_a_final_state=0)
fsm.add_with_update_state(pattern_state_machine_state= 230,byte=0x30,new_state= 120,is_new_state_a_final_state=0)


bfrt.complete_operations()

# Final programming
print("""
******************* PROGAMMING RESULTS *****************
""")

print ("Table forward table:")
forward_table.dump(table=True)

print ("Table fsm:")
fsm.dump(table=True)

