//SwitchIngress code

control Ingress(
    /* User */
    inout ingress_headers_t                       hdr,
    inout ingress_metadata_t                      meta,
    /* Intrinsic */
    in    ingress_intrinsic_metadata_t               ig_intr_md,
    in    ingress_intrinsic_metadata_from_parser_t   ig_prsr_md,
    inout ingress_intrinsic_metadata_for_deparser_t  ig_dprsr_md,
    inout ingress_intrinsic_metadata_for_tm_t        ig_tm_md)
{
// Declare register that will pass results from cloned analyzed packet to original recirculated packet
// temparary variable to store value.
bit<32> let_it_go_register_key=0;
bit<8> let_it_go_register_value=0;
bit<16> port_value=0;
bit<2> accept_state_flag=0;

//Registers
    Register<bit<8>, bit<32>>(400000000) let_it_go_register;
    Register<bit<16>, bit<32>>(400000000) port_register;
/*
    RegisterAction<bit<4>, bit<32>, bit<4>>(let_it_go_register) let_it_go_register_action = {
        void apply(inout bit<4> val, out bit<4> rv) {
            rv = val;
            val = let_it_go_register_value;
        }
    };

    let_it_go_register_value = CLONED_RECIRCULATED_PACKET_FAIL;
    let_it_go_register_action.execute(let_it_go_register_key);
*/

//Hash to compute the key (location) where let_it_go register value will be stored.
Hash<bit<32>>(HashAlgorithm_t.CRC32) hash_1;
//Hash<bit<32>>(HashAlgorithm_t.CRC32) hash_2;
// Forwarding table and action
    action route(PortId_t port, bit<48> smac, bit<48> dmac) {
        ig_tm_md.ucast_egress_port = port;
        //hdr.ethernet.src_addr = smac;
        //hdr.ethernet.dst_addr = dmac;
        //hdr.ipv4.ttl = hdr.ipv4.ttl - 1;
        ig_dprsr_md.drop_ctl = 0;
        }
    action nop() {}

    action drop() {
        ig_dprsr_md.drop_ctl = 0x1; // Drop packet.
    }

    action do_recirculate(){
    ig_tm_md.ucast_egress_port = 68;
    }

    table forward {
        key = {
            hdr.ipv4.dst_addr : lpm;
        }

        actions = {
            route;
            drop;
            nop;
        }
        const default_action = drop;
        size = 1024;
    }

// FSM table and action
    action update_state(bit<16> new_state, bit<2> is_new_state_a_final_state){
    	hdr.recir.pattern_state_machine_state = new_state;
        accept_state_flag = is_new_state_a_final_state;
    }
    action reset_state(){
    	hdr.recir.pattern_state_machine_state = 0;
    }

    table fsm {
        key = {
	     hdr.recir.pattern_state_machine_state:exact;
            hdr.app.byte: exact;
        }

        actions = {
            update_state;
	        reset_state;
            nop;
        }
        const default_action = reset_state();
        size = 1024;
    }

    apply {
      // Find Register index/key/location calculation to store the let_it_go flag (Hash)
        if(hdr.tcp.isValid()){
        let_it_go_register_key = hash_1.get(
           {hdr.ipv4.src_addr,
            hdr.ipv4.dst_addr,
            hdr.tcp.src_port,
            hdr.tcp.dst_port,
            hdr.ipv4.total_len,
            hdr.ipv4.identification})[31:0];
        }

        if(hdr.udp.isValid()){
        let_it_go_register_key = hash_1.get(
           {hdr.ipv4.src_addr,
            hdr.ipv4.dst_addr,
            hdr.udp.src_port,
            hdr.udp.dst_port})[31:0];
        }


        if (hdr.ipv4.isValid()) {
            forward.apply();
        }

        // Orignal packet from ingress (Packet zero state)
        if (hdr.app.isValid() && !hdr.recir.isValid()) {
        // A. Mirror
            ig_dprsr_md.mirror_type = MIRROR_TYPE_I2E;
        // B. Recirculate
            do_recirculate();
        // C. Validate recirculated header
            hdr.recir.setValid();
            hdr.recir.packet_state=0;
            hdr.recir.pattern_state_machine_state=0;

        // D. Set TCP/UDP to RECIRCULAR_PORT
        //    Store the TCP/UDP port value in port register
            if(hdr.tcp.isValid()){
            port_register.write(let_it_go_register_key, hdr.tcp.dst_port);
            hdr.tcp.dst_port = 5555;
            }
            if(hdr.udp.isValid()){
            port_register.write(let_it_go_register_key, hdr.udp.src_port);
            hdr.udp.src_port = 5555;
            }

        // E. Put RECIRCULATE state in let_it_go_register
            let_it_go_register.write(let_it_go_register_key,RECIRCULATE);
        }

        // Normal recirculated Packet (Packet 1 state)
        if (hdr.recir.isValid() && hdr.recir.packet_state == 1){
        // A. Check result of the clone packet from let_it_go_register.
           let_it_go_register_value = let_it_go_register.read(let_it_go_register_key);

        // B. CASE 1: ACCEPT | Remove the recir header
        //    Restore the TCP/UDP port value as well
            if (let_it_go_register_value == ACCEPT){
            hdr.recir.setInvalid();
            port_value = port_register.read(let_it_go_register_key);
            if(hdr.tcp.isValid()){
            hdr.tcp.dst_port = port_value;
            }
            if(hdr.udp.isValid()){
            hdr.udp.src_port = port_value;
            }
            }

        // C. CASE 2: REJECT | drop the packet
            if (let_it_go_register_value == REJECT){
            drop();
            }
        // D. CASE 3: RECIRCULATE | recirculate the packet
            if (let_it_go_register_value == RECIRCULATE){
            do_recirculate();
            }
        }

        // Cloned recirculated Packet (Packet 10 state)
        if (hdr.recir.isValid() && hdr.recir.packet_state == 10){
        // A. Check if app is consumed and let_it_go is in RECIRCULATE state
           let_it_go_register_value = let_it_go_register.read(let_it_go_register_key);
           if(!hdr.app.isValid() && let_it_go_register_value == RECIRCULATE){
             let_it_go_register.write(let_it_go_register_key,ACCEPT);
             drop();
           }
        // B. If app is valid then check FSM table for pattern matching at app layer
           if(hdr.app.isValid()){
           fsm.apply();
           }
        // C. If pattern is found then push REJECT state in let_it_go register
           if(accept_state_flag == 1){
             let_it_go_register.write(let_it_go_register_key,REJECT);
             drop();
           }
        // D. Check register for current state of packet. If recircular then recirculate
           if(hdr.app.isValid() && let_it_go_register_value == RECIRCULATE){
             do_recirculate();
           }
        // E. Truncate the app header
            hdr.app.setInvalid();
        }

    }
}
